ocrmypdf (16.7.0+dfsg1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 16.7.0+dfsg1. (Closes: #1090112)
  * d/copyright: Add info from new version.
  * Remove dependency on python3-pkg-resources. (Closes: #1083514)
  * Build with hatchling.

 -- Bastian Germann <bage@debian.org>  Sun, 05 Jan 2025 14:23:07 +0000

ocrmypdf (16.3.1+dfsg1-1) unstable; urgency=medium

  * Team upload.
  * [3859e89] d/watch: Introduce repacksuffix.
  * [c0b3c4e] New upstream version 16.3.1+dfsg1. (Closes: #1070822)
  * [ed3a601] d/copyright: Update year.

 -- Bastian Germann <bage@debian.org>  Wed, 22 May 2024 21:30:34 +0000

ocrmypdf (16.1.2+dfsg1-1) unstable; urgency=medium

  * [ab69dd4] New upstream version 16.1.2+dfsg1
  * [023612d] Update patch
  * [31332ef] Replace jbig2dec by jbig2. (Closes: #1054243)
  * [945b537] Remove python3-pypdf2 from d/t/control. (Closes: #1061264)
  * [26285ea] Set minimal pikepdf version to 8.14.0

 -- Anton Gladky <gladk@debian.org>  Sun, 31 Mar 2024 15:25:33 +0200

ocrmypdf (15.2.0+dfsg1-1) unstable; urgency=medium

  * [a0abc70] Add gitlab-ci.
  * [4aa3414] New upstream version 15.2.0+dfsg1. (Closes: #1031338, #1030339)
  * [198ab01] Refresh patch.
  * [3230b67] Bump debhelper from old 10 to 13.
  * [0bfa5a3] Set debhelper-compat version in Build-Depends.
  * [1a31bc8] Update renamed lintian tag names in lintian overrides.
  * [12bc393] Set upstream metadata fields:
              Bug-Database, Bug-Submit, Repository, Repository-Browse.
  * [e647baf] Update changelog.
  * [f61eaee] Update VCS. (Closes: #1031337)
  * [f9d894a] Recommend jbig2dec. (Closes: #1054243)
  * [88d101a] Drop dependency on python3-setuptools-scm-git-archive.
              (Closes: #1050101)
  * [d5b43fe] Add python3-hypothesis to depends.

 -- Anton Gladky <gladk@debian.org>  Fri, 20 Oct 2023 07:31:03 +0200

ocrmypdf (14.0.1+dfsg1-1) unstable; urgency=medium

  * [41687f7] Remove gbp.conf
  * [e9a3ecf] Add .gitlab-ci.yml
  * [7779546] Set DPT as maintainer and myself as uploader
  * [448d9c0] New upstream version 14.0.1+dfsg1.
              (Closes: #1023273, #1023463, #1024146)
  * [617fc59] Refresh patches

 -- Anton Gladky <gladk@debian.org>  Sun, 20 Nov 2022 09:46:26 +0100

ocrmypdf (13.7.0+dfsg-2) unstable; urgency=medium

  * QA upload.
  * Set maintainer to Debian QA Group <packages@qa.debian.org>. (see: #1017872)

 -- Marcos Talau <talau@debian.org>  Wed, 16 Nov 2022 07:39:15 -0300

ocrmypdf (13.7.0+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Add build-dep on python3-setuptools-scm-git-archive.

 -- Sean Whitton <spwhitton@spwhitton.name>  Tue, 16 Aug 2022 14:25:29 -0700

ocrmypdf (13.4.3+dfsg-1) unstable; urgency=medium

  * New upstream release.

 -- Sean Whitton <spwhitton@spwhitton.name>  Fri, 15 Apr 2022 14:37:53 -0700

ocrmypdf (13.4.0+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Require pikepdf >=5.0.1 as a simple means to exclude 5.0.0.

 -- Sean Whitton <spwhitton@spwhitton.name>  Wed, 02 Mar 2022 10:33:06 -0700

ocrmypdf (13.2.0+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Drop dependencies on python3-cffi and liblept5.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 23 Dec 2021 15:51:37 -0700

ocrmypdf (12.7.0+dfsg-2) unstable; urgency=medium

  * Tighten build-dep & dep on pikepdf to require >=2.10.0.

 -- Sean Whitton <spwhitton@spwhitton.name>  Fri, 22 Oct 2021 01:07:14 -0700

ocrmypdf (12.7.0+dfsg-1) unstable; urgency=medium

  * New upstream release.
    - Includes updates to d/copyright for license change GPL->MPL.
      See <https://github.com/jbarlow83/OCRmyPDF/issues/600#issuecomment-665998486>
      for full details.  Many thanks to upstream for carefully verifying the
      legitimacy of the relicensing and for updating d/copyright.
  * Add build-deps on python3-importlib-metadata, python3-sphinx-issues.
  * Drop python3-pytest-helpers-namespace from autopkgtest dependencies.
    No longer required by upstream's test suite.
  * Patch pyproject.toml to drop 'addopts' setting which breaks autopkgtest.
    See <https://github.com/jbarlow83/OCRmyPDF/issues/847>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Mon, 18 Oct 2021 17:21:07 -0700

ocrmypdf (10.3.1+dfsg-1) unstable; urgency=medium

  * New upstream release (Closes: #962872).
  * Add tests/plugins/* and misc/watcher.py to d/copyright.
  * Add comment to tests/resources/typewriter.png stanza
    Thanks to upstream.
  * Add deps on python3-pluggy and python3-coloredlogs.

 -- Sean Whitton <spwhitton@spwhitton.name>  Mon, 27 Jul 2020 10:30:04 -0700

ocrmypdf (9.8.0+dfsg-1) unstable; urgency=medium

  * New upstream release.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sun, 17 May 2020 17:21:32 -0700

ocrmypdf (9.7.2+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Drop old files, subsequently deleted by upstream, from Files-Excluded.

 -- Sean Whitton <spwhitton@spwhitton.name>  Wed, 15 Apr 2020 16:36:40 -0700

ocrmypdf (9.6.0+dfsg-1) unstable; urgency=medium

  * New upstream release.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sat, 29 Feb 2020 09:02:01 -0700

ocrmypdf (9.5.0+dfsg-1) unstable; urgency=medium

  * New upstream release.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sun, 02 Feb 2020 19:39:00 -0700

ocrmypdf (9.4.0+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Update d/copyright for new files.
  * Drop dependency on qpdf.

 -- Sean Whitton <spwhitton@spwhitton.name>  Fri, 17 Jan 2020 11:28:30 -0700

ocrmypdf (9.1.0+dfsg-1) unstable; urgency=medium

  * New upstream release (Closes: #944534).
    - Tighten build-dep on python3-pikepdf to require 1.7.0.

 -- Sean Whitton <spwhitton@spwhitton.name>  Tue, 12 Nov 2019 10:00:24 -0700

ocrmypdf (9.0.3+dfsg-1) unstable; urgency=medium

  * New upstream release (Closes: #939044, #940497).
  * Add tesseract-ocr-deu to autopkgtest deps (Closes: #935749).

 -- Sean Whitton <spwhitton@spwhitton.name>  Wed, 18 Sep 2019 16:32:25 -0700

ocrmypdf (9.0.1+dfsg-1) unstable; urgency=medium

  * New upstream release (Closes: #934035).
    - Tighten build-dep on pikepdf to require 1.6.1.
    - Drop deps on ruffus.
    - Drop code in d/rules which passed --force to setup.py.
  * Add docs/images/logo* and misc/media/logo.afdesign to Files-Excluded.
    I cannot confirm that we can regenerate the files in docs/images/ from
    the .afdesign file using tools in Debian main, which latter is
    presumably the preferred form for modification.
  * Newly account for the following files in d/copyright:
    - misc/webservice.py
    - docs/images/bitmap_vs_svg.svg
    - tests/resources/link.pdf
  * Install new bash & fish completion.
    - New build-dep on dh-exec.
  * In response to upstream commit 56a56a4dcb:
    - drop d/rules code to prepare an importable copy of ocrmypdf for the
      docs build
    - patch docs/conf.py to use os.environ['DEB_VERSION_UPSTREAM'] instead
      of calling pkg_resources.get_distribution, which won't work before
      ocrmypdf is installed.

 -- Sean Whitton <spwhitton@spwhitton.name>  Fri, 16 Aug 2019 10:36:12 +0100

ocrmypdf (8.0.1+dfsg-1) unstable; urgency=medium

  * New upstream release.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sat, 26 Jan 2019 13:18:54 -0700

ocrmypdf (8.0.0+dfsg-3) unstable; urgency=medium

  * Require python3-pdfminer (>= 20181108+dfsg-3).

 -- Sean Whitton <spwhitton@spwhitton.name>  Mon, 14 Jan 2019 19:27:28 -0700

ocrmypdf (8.0.0+dfsg-2) unstable; urgency=medium

  * Revert changes in previous upload that disabled usage of pdfminer.six.
    It turns out that the blocking problem was not #886291, but instead
    the problem fixed by the 20181108+dfsg-3 upload of src:pdfminer.
    Thanks to Daniele Tricoli for the fix.

 -- Sean Whitton <spwhitton@spwhitton.name>  Mon, 14 Jan 2019 19:19:45 -0700

ocrmypdf (8.0.0+dfsg-1) unstable; urgency=medium

  * New upstream release.
    - Add tests/resources/enron1.pdf to Files-Excluded
      See https://github.com/pikepdf/pikepdf/issues/21
    - Patch out test_prevent_gs_invalid_xml
      This test requires tests/resources/enron1.pdf
    - Tighten dependency on tesseract-ocr.
    - Tighten {build-,}dep on pikepdf.
  * Drop dependencies on python3-pdfminer & patch pdfminer.six out of setup.py.
    OCRmyPDF's usage of pdfminer is broken due to #886291.  The problem is
    not likely to be fixed in time for the buster freeze, so disable
    pdfminer functionality for now.
    Also see https://github.com/jbarlow83/OCRmyPDF/issues/339
  * Drop bogus Debian changes to upstream file tests/test_main.py by
    checking out the file from tag v8.0.0+dfsg (Closes: #918891).
    The changes were introduced in upstream releases 6.2.4 and 6.2.5 and
    dropped by 7.4.0.  The merge of upstream version 7.4.0 into the Debian
    packaging branch was not done correctly, such that the changes
    remained.

 -- Sean Whitton <spwhitton@spwhitton.name>  Fri, 11 Jan 2019 17:49:29 -0700

ocrmypdf (7.4.0-3) unstable; urgency=medium

  * Upload to unstable.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sun, 06 Jan 2019 20:12:22 +0000

ocrmypdf (7.4.0-2) experimental; urgency=medium

  * Regenerate manpage.

 -- Sean Whitton <spwhitton@spwhitton.name>  Fri, 04 Jan 2019 17:38:59 +0000

ocrmypdf (7.4.0-1) experimental; urgency=medium

  * New upstream release.
    - Tighten {build-,}deps on python3-img2pdf, python3-pikepdf, python3-ruffus
    - Drop python3-libxmp build-dep and autopkgtest dep
    - Add python3-pdfminer versioned {build-,}dep.
    - Add python3-cffi autopkgtest dep.
  * In override_dh_auto_build, delete the line `from . import leptonica`
    from debian/.debhelper/ocrmypdf/__init__.py.
    The directory debian/.debhelper/ocrmypdf is just a hack so that
    upstream's doc build can find the version number, and the cffi setup
    does not work inside debian/.debhelper/ocrmypdf, so avoid the dlopen
    attempt.

 -- Sean Whitton <spwhitton@spwhitton.name>  Fri, 04 Jan 2019 14:51:06 +0000

ocrmypdf (7.2.1-1) experimental; urgency=medium

  * New upstream release.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sat, 20 Oct 2018 13:04:40 -0700

ocrmypdf (7.2.0-2) experimental; urgency=medium

  * Add pngquant to autopkgtest deps.

 -- Sean Whitton <spwhitton@spwhitton.name>  Wed, 10 Oct 2018 17:42:10 -0700

ocrmypdf (7.2.0-1) experimental; urgency=medium

  * New upstream release.
  * Patch setup.py to not require setuptools_scm_git_archive.
    Pending the resolution of #910742.

 -- Sean Whitton <spwhitton@spwhitton.name>  Wed, 10 Oct 2018 08:38:26 -0700

ocrmypdf (7.0.2-1) experimental; urgency=medium

  * New upstream release.
    - Recommend pngquant
    - Require qpdf 8.0.2
    - Add python3-pikepdf, python3-pycparser build-deps
    - Add python3-libxmp build-dep & autopkgtest dep
    - Move python3-pypdf2 build-dep to autopkgtest deps.
    - Drop python3-defusedxml build-dep
    - Drop python3-pytest-timeout autopkgtest dep.
  * Update d/copyright for replacement LinnSequencer.jpg->linn.png.
  * Drop README.Debian
    This version of OCRmyPDF does not use PyMuPDF.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sun, 02 Sep 2018 10:25:25 -0700

ocrmypdf (6.2.5-2) unstable; urgency=medium

  * Fix path in debian/clean (Closes: #913623).
    Thanks to Graham Inggs for the report and patch.

 -- Sean Whitton <spwhitton@spwhitton.name>  Wed, 14 Nov 2018 09:24:33 -0700

ocrmypdf (6.2.5-1) unstable; urgency=medium

  * New upstream release (Closes: #912001).
    Thank you to upstream for the fix.

 -- Sean Whitton <spwhitton@spwhitton.name>  Mon, 29 Oct 2018 17:23:44 -0700

ocrmypdf (6.2.4-1) unstable; urgency=medium

  * New upstream release (Closes: #908937).
    Thank you to upstream for backporting the fix.

 -- Sean Whitton <spwhitton@spwhitton.name>  Mon, 17 Sep 2018 06:52:05 -0700

ocrmypdf (6.2.3-1) unstable; urgency=medium

  * New upstream release (Closes: #904185).
    Thank you to upstream for the fix.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 02 Aug 2018 14:01:46 +0800

ocrmypdf (6.2.2-2) unstable; urgency=medium

  * Tighten {build-,}dependency on python3-ruffus to require 2.7 (Closes:
    #903811).
    Thanks to Paul Gevers for the report.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sun, 15 Jul 2018 18:15:58 +0100

ocrmypdf (6.2.2-1) unstable; urgency=medium

  * New upstream release.
  * Loosen {build-,}dependency on python3-ruffus (Closes: #903627).

 -- Sean Whitton <spwhitton@spwhitton.name>  Fri, 13 Jul 2018 17:28:42 +0100

ocrmypdf (6.2.0-1) unstable; urgency=medium

  * New upstream release.
    - Tighten dependency on qpdf to 7.0.0 or newer.
  * Fix excluding installation instructions from bin:ocrmypdf-doc.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sat, 05 May 2018 12:12:22 -0700

ocrmypdf (6.1.2-1) unstable; urgency=low

  * New upstream release (Closes: #888917).
  * Various updates to d/copyright due to project relicensing and source
    tree rearrangement.
    - Additionally update upstream contact e-mail address.
    - Additionally use https for Format: field.
  * Add python3-defusedxml build-dep.
  * Drop python3-pytest-xdist autopkgtest dependency.
  * Drop SETUPTOOLS_SCM_PRETEND_VERSION hack from d/rules.
    Obsoleted by upstream changes.
  * Update override_dh_auto_build for source tree rearrangement.
  * Update d/tests/control for source tree rearrangement.
  * Add README.Debian about the lack of PyMuPDF support.
  * Add debian/NEWS to detail breaking changes in command line interface.
    Breaking changes in the ocrmypdf library are not detailed because
    ocrmypdf is not considered to provide a stable public API.
  * Expand reasoning in first bullet point of 5.5-2 changelog entry.
  * Patch setup.py to remove addopts key under tool:pytest section.
    The '-n' command line option is not supported by recent pytest.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sat, 31 Mar 2018 11:30:50 -0700

ocrmypdf (5.5-2) unstable; urgency=medium

  * Disable test suite at package build time.
    Rely on autopkgtest instead.  The test suite now takes a prohibitively
    long time to run; upstream expects it to be run after OCRmyPDF is
    installed so running it during the build relies on fragile code in
    d/rules; and it requires a number of heavy build dependencies which
    makes it less convenient to build the package, and to backport the
    package to Debian stable.
  * Move test suite dependencies d/control -> d/tests/control.
  * Set PYBUILD_INSTALL_ARGS to pass --force to setup.py.
    This prevents the build from aborting because tools like unpaper, qpdf
    etc. are not installed.  These programs are not actually needed to
    build the package.
  * Demote unpaper Depends -> Recommends.
    Upstream considers it to be optional.
  * Add --locale to help2man call in gen-man-page target.
  * Regenerate manpage.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sat, 27 Jan 2018 12:10:23 -0700

ocrmypdf (5.5-1) unstable; urgency=medium

  * New upstream release.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sat, 20 Jan 2018 12:22:12 -0700

ocrmypdf (5.4.4-1) unstable; urgency=medium

  * New upstream release.
  * Add new build-dep for test suite: python3-pytest-timeout.
  * Update sed(1) call in override_dh_auto_build for changes to __init__.py.
  * Update d/copyright.
    - Upstream have listed Julien Pfefferkorn in LICENSE.rst but the diff
      between upstream releases shows that he holds copyright on
      hocrtransform.py alone.  Thus, he is not listed under "Files: *".
  * Declare compliance with Debian Policy 4.1.2.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sat, 16 Dec 2017 11:48:57 -0700

ocrmypdf (5.4-1) unstable; urgency=medium

  * New upstream release.
  * Drop Testsuite: field.
    See Lintian tag unnecessary-testsuite-autopkgtest-header.
  * Bump standards version to 4.1.1 (no changes required).

 -- Sean Whitton <spwhitton@spwhitton.name>  Sat, 14 Oct 2017 10:46:45 -0700

ocrmypdf (5.3.2-1) unstable; urgency=medium

  * New upstream release (LP: #1687308).
    - New test suite dependencies: pytest-helpers-namespace; pytest-cov;
      pytest-xdist
  * d/rules:
    - update path to upstream's release notes
    - use $DEB_VERSION_UPSTREAM instead of dpkg-parsechangelog
    - export LC_ALL=C.UTF-8
      The upstream build (and especially test suite) now requires a
      Unicode locale.
      For general information: https://bugs.python.org/issue19846
  * d/copyright:
    - Drop stanza for OCRmyPDF.sh.
      No longer included in upstream's release.
    - Merge stanzas for James R. Barlow & "The OCRmyPDF Authors".
    - Add entries for poster.pdf, overlay.pdf, baiona*.png.
    - Add stanza for pdfa.py.
    - Bump copyright years.
  * Drop patch to test_main.py.
  * Backport upstream commit 82ebd8e to fix a failing test.
  * Bump standards version to 4.1.0 (no changes required).

 -- Sean Whitton <spwhitton@spwhitton.name>  Fri, 01 Sep 2017 13:51:02 -0700

ocrmypdf (4.3.5-3) unstable; urgency=high

  * Backport upstream's workaround for Ghostscript 9.20 "VMerror (-25)"
    (upstream commit e71e8ca) (Closes: #861574).

 -- Sean Whitton <spwhitton@spwhitton.name>  Sun, 30 Apr 2017 16:21:55 -0700

ocrmypdf (4.3.5-2) unstable; urgency=medium

  * Remove restriction on python3-pil build-dep & dep.
    OCRmyPDF works with both pillow 3.4.2 and pillow 4.0.0.  The
    restriction was to avoid python3-pil 4.0.0-1 and 4.0.0-2, which are no
    longer in any Debian suite.

 -- Sean Whitton <spwhitton@spwhitton.name>  Tue, 24 Jan 2017 11:50:32 -0700

ocrmypdf (4.3.5-1) unstable; urgency=medium

  * New upstream release.
  * Tighten python3-pil build-dep & dep to >= 4.0.0-3 (Closes: #851011).

 -- Sean Whitton <spwhitton@spwhitton.name>  Wed, 11 Jan 2017 14:21:08 -0700

ocrmypdf (4.3.4-2) unstable; urgency=medium

  Binary packages:
  * bin:ocrmypdf now suggests img2pdf.
  * Remove claim from --help output and manpage that the img2pdf utility
    is installed as a dependency of ocrmypdf.

  Source package:
  * Drop obsolete README.source.
    All DFSG-freeness issues resolved a while ago.
  * Switch to dgit-maint-merge(7) workflow.
    - Add debian/source/patch-header.
    - Set single-debian-patch & auto-commit in d/source/options.
  * Update Vcs-*.

 -- Sean Whitton <spwhitton@spwhitton.name>  Mon, 02 Jan 2017 23:01:10 +0000

ocrmypdf (4.3.4-1) unstable; urgency=medium

  * New upstream release.
  * Add d/copyright stanza for tests/resources/{typewriter.png,2400dpi.pdf}

 -- Sean Whitton <spwhitton@spwhitton.name>  Sat, 17 Dec 2016 17:08:35 +0000

ocrmypdf (4.3.3-1) unstable; urgency=medium

  * New upstream release (Closes: #846684).
  * Refresh patch-docs-for-Debian.patch.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sat, 03 Dec 2016 08:30:51 -0700

ocrmypdf (4.3.2-2) unstable; urgency=medium

  * Try upload again: sponsor's debsign failed after push to dgit-repos.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sat, 12 Nov 2016 10:31:06 -0700

ocrmypdf (4.3.2-1) unstable; urgency=medium

  * New upstream release.
    - Remove dropped tasks.py from d/copyright.
    - Add new tests/resources/rotated_skew.pdf to d/copyright.
    - Move tests/resources/skew{,-encrypted}.pdf under correct stanza.
      They are based on LinnSequencer.jpg, not a public domain resource.
  * New ocrmypdf-doc binary package: upstream's new HTML documentation.
    - Set PYBUILD_DESTDIR in d/rules.
    - Add override_dh_{auto_build,installdocs,sphinxdoc} targets to d/rules.
    - ocrmypdf suggests python-watchdog.
      See "Cookbook" documentation entry.
    - New build dependencies:
      - python3-sphinx
      - python3-sphinx-rtd-theme
  * Add doc-base registration.
  * Add patch-docs-for-Debian.patch
  * Add path-to-docs-for-Debian.patch
  * Add pip-to-apt-get.patch
  * Bump debhelper compat & build-dep to 10.
  * Run wrap-and-sort -abst.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sat, 12 Nov 2016 09:15:32 -0700

ocrmypdf (4.2.5-1) unstable; urgency=medium

  * New upstream release.
    - Add copyright file entry for tests/resources/epson.pdf.
  * Drop disable-milk.pdf-test.patch.
    Non-free milk.pdf has been replaced by free epson.pdf.
  * Drop pytest-exclude-.git.patch.
    Merged upstream.

 -- Sean Whitton <spwhitton@spwhitton.name>  Fri, 21 Oct 2016 13:45:53 -0700

ocrmypdf (4.2.4+dfsg-1) unstable; urgency=medium

  * New upstream release.
    - Add copyright file entries for several new files.
  * Drop all existing patches.
    Obsoleted by upstream changes.
  * Add disable-milk.pdf-test.patch
  * Add pytest-exclude-.git.patch

  Build:
  * Run test suite with pytest-runner, bypassing pybuild buildsystem.
    This is necessary to successfully run the test suite in advance of
    ocrmypdf being installed.  Previously we used OCRmyPDF.sh to achieve
    this, but that script has been deprecated by upstream.
    - New build dependency on python3-pytest-runner.

  autopkgtest:
  * Run py.test-3 rather than `python3 setup.py test`.
    - Drop obsolete STDERR filtering.
  * Ensure we are testing the installed package, by renaming the 'ocrmypdf'
    directory for the duration of the tests.
  * Drop superfluous dependencies dpkg-dev & python3-pytest-runner.

  Metadata:
  * Add Files-Excluded field to d/copyright.
    Handle new non-free test resource in upstream releases.
  * Rewrite d/watch to pull from GitHub repository, not PyPi.
    This is where I have always actually been packaging from.
  * Bump copyright years.
  * Update README.source.
  * Point Vcs-* at the dgit-repos.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 15 Sep 2016 08:20:54 -0700

ocrmypdf (4.2.2-1) unstable; urgency=medium

  * New upstream release.
  * Append to 0001-patch-test-suite-executable.patch for new stdin test.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sat, 27 Aug 2016 07:36:15 -0700

ocrmypdf (4.2.1+git.20160824.1.5d67cc7-1) unstable; urgency=medium

  * New upstream release.
  * Add missing dependency on python3-pkg-resources (Closes: #835272).
    Thanks to James Murphy for reporting this.
    Eventually the dependency should be included in setup.py: see
    https://github.com/jbarlow83/OCRmyPDF/issues/87

 -- Sean Whitton <spwhitton@spwhitton.name>  Wed, 24 Aug 2016 17:47:16 -0700

ocrmypdf (4.2-1) unstable; urgency=medium

  * New upstream release.
  * Tighten build-dependencies on python3-pypdf2 and python3-img2pdf.
    Upstream changes mean these dependencies are tightened in
    ${python3:Depends}.  Tightening the build-deps too ensures the test
    suite will run correctly at package build time.
  * Drop bash autopkgtest dependency.
    Shouldn't depend on "Essential: yes" packages.
  * Refresh 0001-patch-test-suite-executable.patch.

 -- Sean Whitton <spwhitton@spwhitton.name>  Wed, 17 Aug 2016 12:45:37 -0700

ocrmypdf (4.1.4-2) unstable; urgency=medium

  * Slightly loosen ghostscript dependency to allow ocrmypdf to migrate
    to Ubuntu.
    Thanks to Gianfranco Costamagna for the diff.

 -- Sean Whitton <spwhitton@spwhitton.name>  Tue, 02 Aug 2016 10:20:56 -0700

ocrmypdf (4.1.4-1) unstable; urgency=medium

  * New upstream release.
  * Add ocrmypdf.egg-info/ to debian/clean.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sun, 31 Jul 2016 09:04:58 -0700

ocrmypdf (4.1.3-1) unstable; urgency=medium

  * New upstream release.
  * Add new tests/resources/encrypted_algo4.pdf to d/copyright.
    Upstream reports that it is based on jbig2.pdf.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sun, 03 Jul 2016 12:43:56 +0900

ocrmypdf (4.1.2-1) unstable; urgency=medium

  * Package new upstream version.
    - New dependency on icc-profiles-free.
    - Use dh_link to replace sRGB.icc with a link to the file
      icc-profiles-free installs (which is identical).
    - Update d/copyright to include sRGB.icc.
      Information lifted from d/copyright of src:icc-profiles.
  * Tighten python3-ruffus dependency to << 2.6.3+dfsh.
    ocrmypdf implements a workaround for a bug present in python3-ruffus
    <=2.6.3.  ocrmypdf's author reports that this workaround should not be
    used with the next release of python3-ruffus, which will contain a fix
    for the bug.
    Thanks to Bernhard R. Link for the '+dfsh' formulation, which ensures
    that new Debian revisions, DFSG repacks (e.g. dfsg2), NMUs and binNMUs
    of version 2.6.3 satisfy the dependency but (e.g.) version 2.6.3.1
    does not.  This is required because python3-ruffus does not appear to
    have a version number policy.
  * Simplify code determining SETUPTOOLS_SCM_PRETEND_VERSION.
  * Patch pytest.ini to exclude .pc.
    Test suite no longer runs `rm -r .pc', which was in violation of DEP-8
    rw-build-tree restriction's requirement that the build tree can be
    returned to its original state with `debian/rules clean'.
  * Use bash pipefail option to ensure adt-run recognises autopkgtest test
    suite failures.

 -- Sean Whitton <spwhitton@spwhitton.name>  Fri, 13 May 2016 12:46:02 -0700

ocrmypdf (4.0.7-2) unstable; urgency=low

  * Switch to depend and build-depend on libjpeg-turbo instead of libjpeg8.
    Thanks to Pino Toscano for the suggestion.  (Closes: #822535)
  * Fix autopkgtest:
    - Set SETUPTOOLS_SCM_PRETEND_VERSION.
    - Use pytest-runner.
    - Delete the .pc directory (and set rw-build-tree restriction).
  * Bump standards version to 3.9.8; no changes required.

 -- Sean Whitton <spwhitton@spwhitton.name>  Wed, 27 Apr 2016 07:45:11 -0700

ocrmypdf (4.0.7-1) unstable; urgency=medium

  * Initial release. (Closes: #813562)

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 03 Mar 2016 22:04:31 -0700
